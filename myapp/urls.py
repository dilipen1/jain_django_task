# from django.contrib import admin
# from django.conf import settings
from django.conf.urls import url

urlpatterns = []

# from .views.user import UserCreateList, UserRetriveUpdateDelete

# urlpatterns.append(url(r'^users$',UserCreateList.as_view()))
# urlpatterns.append(url(r'^users/(?P<user_id>\d)$',UserRetriveUpdateDelete.as_view()))

from .views.meeting_room import MeetingRoomCreateList, MeetingRoomRetriveUpdateDelete

urlpatterns.append(url(r'^meeting-rooms$', MeetingRoomCreateList.as_view()))
urlpatterns.append(url(r'^meeting-rooms/(?P<meeting_room>\d)$', MeetingRoomRetriveUpdateDelete.as_view()))

from .views.meeting_room_available import MeetingRoomAvailable

urlpatterns.append(url(r'^meeting-rooms-available$', MeetingRoomAvailable.as_view()))

from .views.booking import BookingCreateList, BookingRetriveUpdateDelete

urlpatterns.append(url(r'^bookings$', BookingCreateList.as_view()))
urlpatterns.append(url(r'^bookings/(?P<booking>\d)$', BookingRetriveUpdateDelete.as_view()))