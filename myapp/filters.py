from rest_framework.filters import BaseFilterBackend
import coreapi

class MeetingRoomAvailableFilterBackend(BaseFilterBackend):
    def get_schema_fields(self, view):
        return [
            coreapi.Field(
                name='q', 
                required=False, 
                location='query', 
                description='Search Query',
                type='string'
            ),
            coreapi.Field(
                name='start_time',
                required=False,
                location='query',
                description='Start Time.',
                type='datetime',
            ),
            coreapi.Field(
                name='end_time',
                required=False,
                location='query',
                description='End Time.',
                type='datetime',
            ),
        ]