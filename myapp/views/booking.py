import json

from django.db.models import Q
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from django.core import serializers

from django.contrib.auth.models import User
# from django.views.decorators.csrf import csrf_exempt
from django.http import Http404
from django.utils.decorators import method_decorator

from rest_framework import mixins, status, viewsets, generics
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.permissions import IsAuthenticated
# from rest_framework.pagination import PageNumberPagination
# from rest_framework.permissions import IsAdminUser

from myapp import serializers
from myapp import models
from myapp.core.pagination import CustomPagination

from myapp.utils.bookings import get_all_available_rooms, get_all_booked_rooms


class BookingCreateList(
		mixins.ListModelMixin,
		mixins.CreateModelMixin,
		generics.GenericAPIView
	):

	permission_classes = (IsAuthenticated,)

	def get_queryset(self):
		return models.Booking.objects.filter(~Q(cancelled=True)).order_by('-id').all()

	def filter_queryset(self, queryset):
		q = Q()

		return queryset.filter(q)

	def get_serializer_class(self):
		return serializers.BookingSerializer

	def get_pagination_class(self, request):
		return CustomPagination

	def get(self, request):
		
		if request.GET.get('verbose') == "true":
			queryset = self.filter_queryset(self.get_queryset())
			serializer = self.get_serializer(queryset, many=True)
			return Response(serializer.data)

		queryset = self.filter_queryset(self.get_queryset())
		page = self.paginate_queryset(queryset)
		if page is not None:
			serializer = self.get_serializer(page, many=True)
			return self.get_paginated_response(serializer.data)
		serializer = self.get_serializer(queryset, many=True)
		return Response(serializer.data)

	def post(self, request):
		serializer_class = self.get_serializer_class()
		data = request.data

		if type(data) is not dict:
			data._mutable = True

		serializer = serializer_class(data=data)
		if not serializer.is_valid():
			return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
		
		available_rooms_queryset = get_all_available_rooms(
			data.get('start_time', None),
			data.get('end_time', None),
		)

		print(available_rooms_queryset.filter(id=data.get('meeting_room', 0)))
		
		meeting_room_available = available_rooms_queryset.filter(id=data.get('meeting_room', 0)).exists()
		if meeting_room_available is False:
			return Response({'message': 'Room already booked.' }, status=status.HTTP_400_BAD_REQUEST)

		booked_rooms_queryset = get_all_booked_rooms(
			data.get('start_time', None),
			data.get('end_time', None),
		)
		meeting_room_booked = booked_rooms_queryset.filter(booking__user=request.user).exists()
		if meeting_room_booked is True:
			return Response({'message': 'You already registered one meeting room.' }, status=status.HTTP_400_BAD_REQUEST)

		serializer.user = request.user
		serializer.save()
		return Response(serializer.data, status=status.HTTP_201_CREATED)


class BookingRetriveUpdateDelete(
		mixins.RetrieveModelMixin,
		# mixins.UpdateModelMixin,
		mixins.DestroyModelMixin,
		generics.GenericAPIView
	):

	permission_classes = (IsAuthenticated,)

	def get_queryset(self):
		return models.Booking.objects.filter(~Q(cancelled=True)).all()

	def filter_queryset(self, queryset):
		q = Q()

		return queryset.filter(q)

	def get_object(self, booking=None):

		if  booking is None:
			return True

		queryset = self.get_queryset()
		queryset = self.filter_queryset(queryset)
		print(queryset.filter(id=booking).first())
		model = get_object_or_404(queryset, id=booking)
		return model

	def get_serializer_class(self):
		return serializers.BookingSerializer

	def get(self, request, booking):
		model = self.get_object(booking)
		serializer_class = self.get_serializer_class()
		serializer = serializer_class(model)
		return Response(serializer.data)
	"""
	def put(self, request, booking):
		model = self.get_object(booking)
		serializer_class = self.get_serializer_class()
		data = request.data

		if type(data) is not dict:
			data._mutable = True

		serializer = serializer_class(model, data=data)
		if not serializer.is_valid():
			return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
		serializer.save()
		return Response(serializer.data, status=status.HTTP_200_OK)

	def patch(self, request, booking):
		model = self.get_object(booking)
		serializer_class = self.get_serializer_class()
		data = request.data

		if type(data) is not dict:
			data._mutable = True

		serializer = serializer_class(model, data=data, partial=True)
		if not serializer.is_valid():
			return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
		serializer.save()
		return Response(serializer.data, status=status.HTTP_200_OK)
	"""
	def delete(self, request, booking):
		model = self.get_object(booking)
		# model.delete()
		model.cancelled = True
		model.save()
		return Response(status=status.HTTP_205_RESET_CONTENT)
