import json
import datetime
from django.utils import timezone
from dateutil.parser import parse as dateutil_parse

from django.db.models import Q
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from django.core import serializers

from django.contrib.auth.models import User
# from django.views.decorators.csrf import csrf_exempt
from django.http import Http404
from django.utils.decorators import method_decorator

from rest_framework import mixins, status, viewsets, generics
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.permissions import IsAuthenticated
# from rest_framework.pagination import PageNumberPagination
# from rest_framework.permissions import IsAdminUser

from myapp import serializers
from myapp import models
from myapp.core.pagination import CustomPagination

from myapp.filters import MeetingRoomAvailableFilterBackend
from myapp.utils.bookings import get_all_bookings


class MeetingRoomAvailable(
		mixins.ListModelMixin,
		generics.GenericAPIView
	):

	filter_backends = (MeetingRoomAvailableFilterBackend,)

	permission_classes = (IsAuthenticated,)

	def get_queryset(self):
		return models.MeetingRoom.objects.order_by('-id').all()

	def filter_queryset(self, request, queryset):

		all_bookings_queryset = get_all_bookings(
			request.GET.get('start_time', None),
			request.GET.get('end_time', None),
		)
		
		q1 = Q()
		if 'q' in request.GET:
			q1 = q1 & Q(name__startswith=request.GET.get('q'))

		return queryset.filter(~Q(booking__in=all_bookings_queryset) & q1)

	def get_serializer_class(self):
		return serializers.MeetingRoomSerializer

	def get_pagination_class(self, request):
		return CustomPagination

	def get(self, request):
		
		if request.GET.get('verbose') == "true":
			queryset = self.filter_queryset(request, self.get_queryset())
			serializer = self.get_serializer(queryset, many=True)
			return Response(serializer.data)

		queryset = self.filter_queryset(request, self.get_queryset())
		page = self.paginate_queryset(queryset)
		if page is not None:
			serializer = self.get_serializer(page, many=True)
			return self.get_paginated_response(serializer.data)
		serializer = self.get_serializer(queryset, many=True)
		return Response(serializer.data)
