from django.contrib.auth.models import User

from rest_framework import serializers
from . import models


class UserSerializer(serializers.ModelSerializer):
    """Serializer to map the Model instance into JSON format."""

    class Meta:
        """Meta class to map serializer's fields with the model fields."""
        model = User
        fields = '__all__'


class UploadFileSerializer(serializers.ModelSerializer):  # NOQA
    """Serializer to map the Model instance into JSON format."""

    # {{my_custom_field_name}} = serializers.SerializerMethodField()

    # def get_{{my_custom_field_name}}(self, model):
    #   return NewModel.objects.all().filter(model=model).count()

    class Meta:
        """Meta class to map serializer's fields with the model fields."""
        model = models.UploadFile
        fields = '__all__'
        # fields = ( 'id', 'created_at', 'modified_at', 'deleted', 'name', 'file', ) # NOQA
        # extra_fields = ('{{my_custom_field_name}}', )
        # read_only_fields = ( 'created_at', 'modified_at', )

class MeetingRoomSerializer(serializers.ModelSerializer):
    """Serializer to map the Model instance into JSON format."""

    # {{my_custom_field_name}} = serializers.SerializerMethodField()

    # def get_{{my_custom_field_name}}(self, model):
        # return NewModel.objects.all().filter(model=model).count()

    class Meta:
        """Meta class to map serializer's fields with the model fields."""
        model = models.MeetingRoom
        fields = '__all__'
        # fields = ( 'booking>', 'id', 'name', 'created_at', 'updated_at', )
        # extra_fields = ('{{my_custom_field_name}}', )
        # read_only_fields = ( 'created_at', )

class BookingSerializer(serializers.ModelSerializer):
    """Serializer to map the Model instance into JSON format."""

    # {{my_custom_field_name}} = serializers.SerializerMethodField()

    # def get_{{my_custom_field_name}}(self, model):
        # return NewModel.objects.all().filter(model=model).count()

    class Meta:
        """Meta class to map serializer's fields with the model fields."""
        model = models.Booking
        # fields = '__all__'
        # fields = ( 'id', 'meeting_room', 'name', 'start_time', 'end_time', 'cancelled', 'created_at', 'updated_at', )
        # extra_fields = ('{{my_custom_field_name}}', )
        # read_only_fields = ( 'created_at', )
        exclude = ('cancelled', )
