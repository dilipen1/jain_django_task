"""
All Model should be an sql table.
"""

# import uuid
from django.db import models
from django.utils import timezone

from django.contrib.auth.models import User
# from django.contrib.auth.validators import ASCIIUsernameValidator


class MeetingRoom(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=200, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Booking(models.Model):
    id = models.AutoField(primary_key=True)
    meeting_room = models.ForeignKey(MeetingRoom, null=True, blank=True, on_delete=models.CASCADE)  # NOQA
    name = models.CharField(max_length=200, blank=True, null=True)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    cancelled = models.BooleanField(default=False, blank=True)
    user = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE)  # NOQA
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class UploadFile(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=200, blank=True, null=True)
    file = models.FileField(max_length=250, blank=True, null=True)
    created_at = models.DateTimeField(default=timezone.now)
    modified_at = models.DateTimeField(default=timezone.now)
    deleted = models.BooleanField(default=False, blank=True)
