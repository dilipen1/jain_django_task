from django.utils import timezone
from dateutil.parser import parse as dateutil_parse

from django.db.models import Q

from myapp import models


def get_all_bookings(start_time=None, end_time=None):
    start_time = timezone.now()
    end_time = timezone.now()

    if start_time is None:
        start_time = dateutil_parse(start_time)
        
    if end_time is None:
        end_time = dateutil_parse(end_time)
    
    q = Q()
    q = q & Q(start_time__lte=start_time)
    q = q & Q(end_time__gte=end_time)

    _return = models.Booking.objects.\
        filter(~Q(cancelled=True) & q).all()

    return _return


def get_all_available_rooms(start_time=None, end_time=None):
    all_bookings = get_all_bookings(start_time=start_time, end_time=end_time)
    _return = models.MeetingRoom.objects.filter(~Q(booking__in=all_bookings))
    return _return

def get_all_booked_rooms(start_time=None, end_time=None):
    all_bookings = get_all_bookings(start_time=start_time, end_time=end_time)
    _return = models.MeetingRoom.objects.filter(Q(booking__in=all_bookings))
    return _return